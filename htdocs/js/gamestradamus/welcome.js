define([
    "atemi/io/jsonrpc",
    "dojo/_base/event",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/on",
    "dojo/query",
    "dojo/request/xhr",
    "dojo/NodeList-dom",
    "dojo/domReady!"
], function(jsonrpc, event, dom, domClass, on, query, xhr) {

    var init = function() {
        var rpc = jsonrpc('/jsonrpc');
        var welcome_started = Date.parse(
            query('div.processing')[0].getAttribute('data-welcome-started')
        );

        on(query('a.disabled'), 'click', function(evt) {
            if (domClass.contains(evt.target, 'disabled')) {
                event.stop(evt);
                return;
            }
        }); 

        // When was the last time the games were updated ?
        var timer;
        var is_game_library_updated = function() {
            rpc.request({
                method: 'predictions.last-game-library-update',
                params: []
            }, function(data) {
                var private_profile_since = Date.parse(data.private_profile_since);
                var last_update = Date.parse(data.last_updated);
                if (!isNaN(private_profile_since) && private_profile_since > welcome_started) {
                    clearInterval(timer);   // stop checking
                    // Show the private profile information.
                    query('div.content').addClass('hidden');
                    query('div.private').removeClass('hidden');
                    query('div.processing div.loader').addClass('complete');
                    query('div.processing div.loader').addClass('private');
                    query('div.processing div.loader')[0].innerHTML = 'Your profile cannot be imported yet.';

                } else if (!isNaN(last_update) && last_update > welcome_started) {
                    clearInterval(timer);   // stop checking

                    query('div.processing div.loader').addClass('complete');
                    query('div.processing div.loader')[0].innerHTML = 'Finished with ' + data.count + ' games imported.';
                    query('a.button').removeClass('disabled');
                } else {
                    return;     // keep checking.
                }
            });
        };
        timer = setInterval(is_game_library_updated, 4000);
        is_game_library_updated();
    };
    return init;
})
