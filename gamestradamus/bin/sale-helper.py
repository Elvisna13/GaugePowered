# coding: utf-8
from decimal import Decimal
import requests
from bs4 import BeautifulSoup

from gamestradamus.channel.steam.spider import AgeCheckSpider


BITLY_ACCESS_TOKEN = ""


#------------------------------------------------------------------------------
def build_tables(steam_urls):
    spider = AgeCheckSpider(0.10)
    games = []

    gaming_wiki_urls = {}
    gaming_wiki_base = "http://pcgamingwiki.com"
    g_wiki_response = requests.get("http://pcgamingwiki.com/wiki/Category:Games")
    soup = BeautifulSoup(g_wiki_response.text)
    for a in soup.select(".mw-content-ltr a"):
        gaming_wiki_urls[a.string] = "%s%s" % (gaming_wiki_base, a['href'])

    for steam_url in steam_urls:
        parts = steam_url.split("/")
        app_id = parts[4]
        gauge_url = "http://www.gaugepowered.com/app/%s/" % app_id

        response = requests.get(gauge_url)
        soup = BeautifulSoup(response.text)
        average_playtime = soup.select('.average-playtime .stat')
        print gauge_url
        average_playtime = Decimal(average_playtime[0].text)

        rating = soup.select('.rating .rating-5 .rating-5')
        rating = Decimal(rating[0].attrs['title'].split('/')[0].strip())

        full_price = None
        percent_off = None
        price = soup.select('.price')
        prices = price[0].text.split("$")
        if len(prices) == 2:
            price = Decimal(prices[1])
        elif len(prices) == 3:
            percent_off = Decimal(prices[0].split("%")[0])
            price = Decimal(prices[1])
            full_price = Decimal(prices[2])
        else:
            print "Couldn't parse price!"
            1/0

        name = soup.select('.info h1')
        name = name[0].text
        gaming_wiki_url = gaming_wiki_urls.get(name, '')


        total_count = 0
        total_count_elems = soup.select(".total-count .stat")
        total_count = int(total_count_elems[0].text.replace(",", ""))


        sale_cph = soup.select('.average-cost-per-hour .on-sale')
        unit = u"$"
        if sale_cph:
            if "$" in sale_cph[0].text:
                splitter = u"$"
                sale_cph = sale_cph[0].text.split(splitter)
                cph = Decimal(sale_cph[1].replace("Cost / Hour (On Sale)", ""))
                sort_cph = cph
                cph = u'$%0.2f' % cph
            else:
                unit = u"¢"
                splitter = u"¢"
                sale_cph = sale_cph[0].text.split(splitter)
                sort_cph = Decimal(sale_cph[0].replace("Cost / Hour (On Sale)", "")) / Decimal('100.0')
                cph = int(sale_cph[0].replace("Cost / Hour (On Sale)", ""))
                cph = u'%s¢' % cph
        else:
            print "gauge hasn't put this thing on sale yet!"
            normal_cph = soup.select('.average-cost-per-hour .stat')
            if normal_cph:
                if u"$" in normal_cph[0].text:
                    splitter = u"$"
                    normal_cph = normal_cph[0].text.split(splitter)
                    cph = Decimal(normal_cph[1].replace("Cost / Hour", ""))
                    sort_cph = cph
                    cph = u'$%0.2f' % cph
                else:
                    splitter = u"¢"
                    unit = u"¢"
                    normal_cph = normal_cph[0].text.split(splitter)
                    sort_cph = Decimal(normal_cph[0].replace("Cost / Hour", "")) / Decimal('100.0')
                    cph = int(normal_cph[0].replace("Cost / Hour", ""))
                    cph = u'%s¢' % cph
            else:
                1/0

        response = spider.GET(steam_url)
        new_response = spider.check_response(steam_url, response)
        if new_response:
            response = new_response
        soup = BeautifulSoup(response.text)
        trading_cards = False
        for element in soup.select(".game_area_details_specs .name"):
            if element.text == "Steam Trading Cards":
                trading_cards = True

        for element in soup.select(".game_area_description h2"):
            if element.text == "Steam Trading Cards":
                trading_cards = True

        linux = False
        linux_elements = soup.select(".platform_img.linux")
        if len(linux_elements) > 0:
            linux = True

        mac = False
        mac_elements = soup.select(".platform_img.mac")
        if len(mac_elements) > 0:
            mac = True

        early_access = ''
        early_access_div = soup.select('.early_access_header')
        if early_access_div:
            early_access = ' (**Early Access**)'

        games.append({
            'name': name,
            'price': u'%0.2f' % price,
            'full_price': u'%0.2f' % full_price,
            'percent_off': u'%0d%%' % percent_off,
            'gauge_url': gauge_url,
            'steam_url': steam_url,
            'gaming_wiki_url': '[Wiki](%s)' % gaming_wiki_url if gaming_wiki_url else '',
            'rating': u'%0.1f / 5 ★' % rating if total_count > 0 else 'NA',
            'average_playtime': u'%s hrs' % average_playtime if total_count > 0 else 'NA',
            'sort_cph': sort_cph,
            'cph': u'%s / hr' % cph if total_count > 0 else 'NA',
            'trading_cards': u'Y' if trading_cards else '',
            'mac': u'Y' if mac else '',
            'linux': u'Y' if linux else '',
            'early_access': early_access,
        })

    games.sort(key=lambda g: g['sort_cph'])


    table = u"""
| Name       | Cost Per Hour    |   Median Hours Played     |    Average Rating   | Cost | Steam | PCGW  | Lin.  | Mac | Cards         |
|:----------:|:----------------:|:-------------------------:|:-------------------:|:----:|:-----:|:-----:|:-----:|:---:|:-------------:|
%s
    """
    row = u"""|[%(name)s](%(gauge_url)s)%(early_access)s|[%(cph)s](%(gauge_url)s)|[%(average_playtime)s](%(gauge_url)s)|[%(rating)s](%(gauge_url)s)|[%(percent_off)s ~~$%(full_price)s~~ $%(price)s](%(steam_url)s)|[Buy](%(steam_url)s)|%(gaming_wiki_url)s|%(linux)s|%(mac)s|%(trading_cards)s|"""

    row_string = "\n".join([row % game for game in games])
    print (table % row_string).encode("utf-8")

    table = u"""
| Name       | Cost | PC Gaming Wiki   | Lin.  | Mac | Cards         |
|:-----------|:----:|:----------------:|:-----:|:---:|:-------------:|
%s
    """
    row = u"""|[%(name)s](%(steam_url)s)%(early_access)s|[%(percent_off)s ~~$%(full_price)s~~ $%(price)s](%(steam_url)s)|%(gaming_wiki_url)s|%(linux)s|%(mac)s|%(trading_cards)s|"""

    row_string = "\n".join([row % game for game in games])
    print (table % row_string).encode("utf-8")

#------------------------------------------------------------------------------
def build_previous_lists(steam_urls):
    spider = AgeCheckSpider(0.10)
    games = []

    for steam_url in steam_urls:
        parts = steam_url.split("/")
        app_id = parts[4]
        gauge_url = "http://www.gaugepowered.com/app/%s/" % app_id

        response = requests.get(gauge_url)
        soup = BeautifulSoup(response.text)
        #print gauge_url

        name = soup.select('.info h1')
        name = name[0].text
        print ("* ~~%s~~" % name).encode("utf-8")

#-------------------------------------------------------------------------------
daily_deals = [
        'http://store.steampowered.com/app/251570/',
        'http://store.steampowered.com/app/240760/',
        'http://store.steampowered.com/app/213670/',
        'http://store.steampowered.com/app/49520/',
        'http://store.steampowered.com/app/244850/',
        'http://store.steampowered.com/app/105600/',
        'http://store.steampowered.com/app/107410/',
        'http://store.steampowered.com/app/233250/',
        'http://store.steampowered.com/app/200510/',
        'http://store.steampowered.com/app/278080/',
        'http://store.steampowered.com/app/238460/',
        'http://store.steampowered.com/app/230230/',
        'http://store.steampowered.com/app/206420/',
        'http://store.steampowered.com/app/211420/',
        'http://store.steampowered.com/app/261030/',
        'http://store.steampowered.com/app/730/',
        'http://store.steampowered.com/app/8870/',
        'http://store.steampowered.com/app/72850/',
        'http://store.steampowered.com/app/203160/',
        'http://store.steampowered.com/app/241540/',
        'http://store.steampowered.com/app/47810/',
        'http://store.steampowered.com/app/901583/',
        'http://store.steampowered.com/app/218620/',
        'http://store.steampowered.com/app/250320/',
        'http://store.steampowered.com/app/267530/',
        'http://store.steampowered.com/app/209000/',
        'http://store.steampowered.com/app/205100/',
        'http://store.steampowered.com/app/242920/',
        'http://store.steampowered.com/app/226840/',
        'http://store.steampowered.com/app/220240/',
        'http://store.steampowered.com/app/225080/',
        'http://store.steampowered.com/app/2028016/',
        'http://store.steampowered.com/app/235460/',
        'http://store.steampowered.com/app/20920/',
        'http://store.steampowered.com/app/220200/',
        'http://store.steampowered.com/app/8930/',
        'http://store.steampowered.com/app/43160/',
        'http://store.steampowered.com/app/214950/',
        'http://store.steampowered.com/app/242050/',
        'http://store.steampowered.com/app/221910/',
]
previous_deals = [
]
flash_deals = [
]
community_choice  = [
]

print "Daily Deals:"
build_tables(daily_deals)

print "Flash Sales:"
build_tables(flash_deals)

print "Community Choices:"

build_tables(community_choice)

build_previous_lists(previous_deals)
